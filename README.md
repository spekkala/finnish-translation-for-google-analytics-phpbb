See below for English description.

# Suomennos Google Analytics -laajennukselle

Suomenkielinen kielipaketti phpBB:n Google Analytics -laajennukselle.

## Asennus

1. Asenna Google Analytics -laajennus ensin.

2. Lataa laajennuksen versiota vastaava kielipaketti alla olevasta luettelosta:

	- [1.0.2 – 1.0.3](https://bitbucket.org/spekkala/finnish-translation-for-google-analytics-phpbb/downloads/google-analytics-fi-1.0.2.zip)
	- [1.0.1](https://bitbucket.org/spekkala/finnish-translation-for-google-analytics-phpbb/downloads/google-analytics-fi-1_0_1.7z)

3. Pura paketin sisältö phpBB:n `ext/phpbb/googleanalytics`-hakemistoon.

# Finnish Translation for Google Analytics

A Finnish language pack for the Google Analytics extension for phpBB.

## Installation

1. Install the Google Analytics extension first.

2. Download the language pack matching the version of the extension from the
list below:

	- [1.0.2 – 1.0.3](https://bitbucket.org/spekkala/finnish-translation-for-google-analytics-phpbb/downloads/google-analytics-fi-1.0.2.zip)
	- [1.0.1](https://bitbucket.org/spekkala/finnish-translation-for-google-analytics-phpbb/downloads/google-analytics-fi-1_0_1.7z)

3. Extract the contents of the archive into the `ext/phpbb/googleanalytics`
directory under your phpBB root directory.

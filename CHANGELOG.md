# Change Log

## 1.0.2 (2018-01-01)

- Update translations to be compatible with the Google Analytics extension
  version 1.0.2.

## 1.0.1 (2015-10-06)

- Initial release.

const gulp = require('gulp');
const gutil = require('gulp-util');

gulp.task('watch', () => {
	gulp.watch('language/fi/**', event => {
		if(event.type === 'changed') {
			gutil.log('Changed file:', event.path);

			gulp.src(event.path, { base: './' })
				.pipe(gulp.dest('../public/ext/phpbb/googleanalytics'));
		}
	});
});
